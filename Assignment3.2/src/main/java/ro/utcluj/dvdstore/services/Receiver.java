package ro.utcluj.dvdstore.services;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeoutException;

/**
 * @author catalin
 */
public class Receiver {
    private final static String QUEUE_NAME = "dvds";

    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println("[*] Waiting for messages. To exit press CTRL+C");
        final MailService mailService = new MailService("eee", "eee");
        final PrintWriter writer = new PrintWriter("dvds.txt", "UTF-8");

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                String message = new String(body, "UTF-8");
                mailService.sendMail("marincas.catalin.zamfir@gmail.com", "New dvd available!", message);
                writer.println(message);
                writer.flush();
                System.out.println("[x] Received '" + message + "'");
            }
        };
        channel.basicConsume(QUEUE_NAME, true, consumer);
    }
}
