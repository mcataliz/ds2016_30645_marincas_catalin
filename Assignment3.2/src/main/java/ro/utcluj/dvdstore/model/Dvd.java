package ro.utcluj.dvdstore.model;

/**
 * @author catalin
 */
public class Dvd {
    private String title;
    private int year;
    private int price;

    public Dvd(String title, int year, int price) {
        this.title = title;
        this.year = year;
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String toString() {
        return "DVD: title=" + title + ", year= " + year + ", price= " + price;
    }
}
