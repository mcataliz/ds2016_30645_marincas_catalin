package ro.utcluj.dvdstore.servlets;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import ro.utcluj.dvdstore.model.Dvd;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeoutException;

/**
 * @author catalin
 */
public class NewDvdServlet extends HttpServlet {
    private final static String QUEUE_NAME = "dvds";

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = "";
        int year = 0;
        int price = 0;

        PrintWriter writer = response.getWriter();
        writer.println("<body>");
        try {
            title = request.getParameter("title");
            year = Integer.parseInt(request.getParameter("year"));
            price = Integer.parseInt(request.getParameter("price"));
        } catch (Exception ex){
            writer.println("<h2>Wrong or missing information!</h2>");
            throw new RuntimeException();
        }

        Dvd dvd = new Dvd(title, year, price);

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = null;
        try {
            connection = factory.newConnection();
            Channel channel = connection.createChannel();

            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            String message = dvd.toString();
            channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
            System.out.println("[x] Sent '" + message + "'");

            channel.close();
            connection.close();

            writer.println("<h2>New dvd (title: " + title + ") successfully added.</h2>");
        } catch (TimeoutException e) {
            writer.println("<h1>Unable to send the information!<h1>");
        }
        writer.println("<form action='index.jsp'>");
        writer.println("<input type='submit' value='Add another'/>");
        writer.println("</form>");
        writer.println("</body>");
        writer.close();
    }
}
