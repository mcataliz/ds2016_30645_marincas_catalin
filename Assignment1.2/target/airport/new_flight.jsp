<%@ page import="ro.utcluj.airport.data.model.City" %>
<%@ page import="java.util.List" %>
<%@ page import="ro.utcluj.airport.data.model.Flight" %>
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<%
    boolean isAdmin = (boolean)session.getAttribute("isAdmin");
    if (!isAdmin) {
        response.sendRedirect(request.getContextPath() + "/flights");
    }
%>
<html>
<head>
    <link rel="stylesheet" href="css/stylesheet.css"/>
</head>
<body>
<%
    DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    List<City> cities = (List<City>) request.getAttribute("cities");
    Flight flight = (Flight) request.getAttribute("flight");

    String flightId = "";
    String airplaneType = "";
    String arrivalTime = "";
    String departureTime = "";
    int departureCity = 0;
    int arrivalCity = 0;

    if (flight != null) {
        flightId = flight.getId() + "";
        airplaneType = flight.getAirplaneType();
        arrivalCity = flight.getArrivalCity().getId();
        departureCity = flight.getDepartureCity().getId();
        departureTime = df.format(flight.getDepartureTime());
        arrivalTime = df.format(flight.getArrivalTime());
    }

%>
<div id="container">
    <div id="header">
        <div id="logo">
            <h1>Skyline <span>Fly with us...</span></h1>
        </div>
        <div id="menu">
            <ul>
                <li><a href="flights">Flights</a></li>
                <li><a href="flights?action=new">Add Flight</a></li>
                <li><a href="logout">Logout</a></li>
            </ul>
        </div>
    </div>
    <div id="content">
        <div id="login" class="new_flight">
            <h1> Add new flight</h1>
            <form method="POST" action="flights">
                <p>Airplane type:</p>
                <input type="text" value="<%=flightId%>" name="flightId" style="display:none"/>
                <input type="text" value="<%=airplaneType%>" name="airplaneType" placeholder="Enter the name of the plane"/>
                <p>Departure city:</p>
                <select name="departureCity">
                    <% for (City city : cities) {
                        if (city.getId() == departureCity) {%>
                    <option value="<%=city.getId()%>" selected><%= city.getName()%></option>
                    <%}else{%>
                    <option value="<%=city.getId()%>"><%= city.getName()%></option>
                    <%}}%>
                </select>
                <p>Departure date
                    <br/>(DD/MM/YYYY HH:MM):</p>
                <input type="text" name="departureDate" value="<%=departureTime%>" placeholder="DD/MM/YYYY HH:MM"/>
                <p>Arrival city:</p>
                <select name="arrivalCity">
                    <% for (City city : cities) {
                        if (city.getId() == arrivalCity) {%>
                    <option value="<%=city.getId()%>" selected><%= city.getName()%></option>
                    <%}else{%>
                    <option value="<%=city.getId()%>"><%= city.getName()%></option>
                    <%}}%>
                </select>
                <p>Arrival date:</p>
                <input type="text" name="arrivalDate" value="<%=arrivalTime%>" placeholder="DD/MM/YYYY HH:MM"/>
                <input type="submit" value="Save"/>
            </form>
        </div>
    </div>
    <div id="footer">
        <p>Assignment 1.2 | Copyright <span>Marincas Catalin</span> | 30645
    </div>
</div>
</body>
</html>