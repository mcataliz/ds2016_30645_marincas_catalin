<%@ page import="ro.utcluj.airport.data.model.Flight" %>
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="css/stylesheet.css" />
</head>
<body>
<%
	List<Flight> flights = (List<Flight>) request.getAttribute("flights");
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	boolean isAdmin = ((boolean)session.getAttribute("isAdmin"));
	String departureLT = (String)request.getAttribute("departureLT");
	String arrivalLT = (String)request.getAttribute("arrivalLT");
	Integer flightId = (Integer)request.getAttribute("flightId");
	String depCity = (String)request.getAttribute("depCity");
	String arrivCity = (String)request.getAttribute("arrivCity");

%>
	<div id="container">
		<div id="header">
			<div id="logo">
				<h1>Skyline <span>Fly with us...</span></h1>
			</div>
			<div id="menu">
					<ul>
						<li><a href="flights">Flights</a></li>
						<%if(isAdmin) {%>
							<li><a href="flights?action=new">Add Flight</a></li>
						<%}%>
						<li><a href="logout">Logout</a></li>
					</ul>
			</div>
		</div>

		<div id="content">
			<% if (departureLT!=null && arrivalLT!=null){ %>
			<div id="notification" class="popup">
				<h1 onclick="document.getElementById('notification').removeAttribute('class')">x</h1>
				<p>Flight Id: -----------------------> <span><%=flightId%></span></p>
				<p>Departure(<u><%=depCity%></u>) local time: <span><%=departureLT%></span></p>
				<p>Arrival(<u><%=arrivCity%></u>) local time: <span><%=arrivalLT%></span></p>
			</div>
			<%}%>

			<div id="flights">
				<div class="flight head">
					<p class="id">Id</p>
					<p>Plane</p>
					<p>Departure City</p>
					<p>Departure Date</p>
					<p>Arrival City</p>
					<p>Arrival Date</p>
					<% if (isAdmin) {%>
					<p class="action-btn">Delete</p>
					<%} else { %>
					<p class="action-btn">Local T</p>
					<%}%>
				</div>

				<% for(Flight flight:flights) {%>
					<div class="flight">
						<%if (isAdmin) {%>
						<a class="edit" href="flights?action=edit&id=<%=flight.getId()%>"><p class="id"><%= flight.getId() %>.</p></a>
						<%}else{%>
						<p class="id"><%= flight.getId() %>.</p>
						<%}%>
						<p><%= flight.getAirplaneType() %></p>
						<p><%= flight.getDepartureCity().getName() %></p>
						<p><%= df.format(flight.getDepartureTime()) %></p>
						<p><%= flight.getArrivalCity().getName() %></p>
						<p><%= df.format(flight.getArrivalTime()) %></p>
						<% if (isAdmin){%>
						<a href="flights?action=delete&id=<%=flight.getId()%>"><p class="action-btn">X</p></a>
						<%}else{%>
						<a href="localtime?id=<%=flight.getId()%>"><p class="action-btn">?</p></a>
						<%}%>
					</div>
				<%}%>
			</div>
		</div>
		<div id="footer">
			<p>Assignment 1.2 | Copyright <span>Marincas Catalin</span> | 30645
		</div>
	</div>
</body>
</html>