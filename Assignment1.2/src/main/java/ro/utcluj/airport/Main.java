package ro.utcluj.airport;

import ro.utcluj.airport.data.dao.CityDAO;
import ro.utcluj.airport.data.dao.FlightDAO;
import ro.utcluj.airport.data.dao.UserDAO;
import ro.utcluj.airport.data.model.City;
import ro.utcluj.airport.data.model.Flight;
import ro.utcluj.airport.data.model.User;

import java.util.Calendar;
import java.util.List;

/**
 * @author catalin
 */
public class Main {

    public static void main(String[] args) {
        UserDAO userDAO = new UserDAO();

        userDAO.saveUser(new User("user", "user", "user", false));
        userDAO.saveUser(new User("admin", "admin", "admin", true));


        City city1 = new City("Paris", 48.864716, 2.349014);
        City city2 = new City("Atena", 37.9838, 23.7275);
        City city3 = new City("Londra", 51.5074, 0.1278);
        City city4 = new City("Bucuresti", 44.4268, 26.1025);
        City city5 = new City("New York", 40.7128, 74.0059);
        CityDAO cityDAO = new CityDAO();
        cityDAO.saveCity(city1);
        cityDAO.saveCity(city2);
        cityDAO.saveCity(city3);
        cityDAO.saveCity(city4);
        cityDAO.saveCity(city5);


        Calendar departure = Calendar.getInstance();
        Calendar arrival = Calendar.getInstance();
        departure.setTimeInMillis(0);
        arrival.setTimeInMillis(0);

        departure.set(2015, 12, 23, 23, 12, 23);
        arrival.set(2015, 12, 25, 13, 12, 00);
        Flight flight1 = new Flight("AIRBUS_A350", city1, departure.getTime(), city2, arrival.getTime() );

        departure.set(2016, 8, 23, 23, 12, 23);
        arrival.set(2016, 9, 1, 13, 00, 00);
        Flight flight2 = new Flight("BOEING_787", city2, departure.getTime(), city1, arrival.getTime() );

        FlightDAO flightDAO = new FlightDAO();
        flightDAO.saveFlight(flight1);
        flightDAO.saveFlight(flight2);
    }
}
