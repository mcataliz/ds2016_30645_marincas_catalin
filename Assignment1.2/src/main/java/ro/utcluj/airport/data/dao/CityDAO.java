package ro.utcluj.airport.data.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import ro.utcluj.airport.data.HibernateUtil;
import ro.utcluj.airport.data.model.City;
import ro.utcluj.airport.data.model.Flight;

import java.util.List;

/**
 * @author catalin
 */
public class CityDAO {

    private static final Logger LOGGER = LogManager.getLogger(FlightDAO.class);

    public City saveCity(City city) {
        int cityId = -1;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            cityId = (Integer) session.save(city);
            city.setId(cityId);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("Unable to save city", e);
        } finally {
            session.close();
        }
        return city;
    }

    @SuppressWarnings("unchecked")
    public List<City> findCities() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        List<City> cities = null;
        try {
            tx = session.beginTransaction();
            cities = session.createQuery("FROM City ").list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("Error in reading cities.", e);
        } finally {
            session.close();
        }
        return cities;
    }

    @SuppressWarnings("unchecked")
    public City findCity(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        List<City> cities = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM City WHERE id = :id");
            query.setParameter("id", id);
            cities = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("Error in reading cities", e);
        } finally {
            session.close();
        }
        return cities != null && !cities.isEmpty() ? cities.get(0) : null;
    }
}
