package ro.utcluj.airport.data.model;

import javax.persistence.*;
import java.util.Date;

/**
 * @author catalin
 */

@Entity
@Table(name = "Flights")
public class Flight {
    @Id
    @GeneratedValue
    @Column(name = "FLIGHT_ID")
    private int id;
    @Column(name = "AIRPLANE_TYPE")
    private String airplaneType;
    @ManyToOne
    @JoinColumn(name = "DEPARTURE_CITY")
    private City departureCity;
    @Column(name = "DEPARTURE_TIME")
    private Date departureTime;
    @ManyToOne
    @JoinColumn(name = "ARRIVAL_CITY")
    private City arrivalCity;
    @Column(name = "ARRIVAL_TIME")
    private Date arrivalTime;

    public Flight(){
    }

    public Flight(String airplaneType, City departureCity, Date departureTime, City arrivalCity, Date arrivalTime) {
        this.airplaneType = airplaneType;
        this.departureCity = departureCity;
        this.departureTime = departureTime;
        this.arrivalCity = arrivalCity;
        this.arrivalTime = arrivalTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }

    public City getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(City departureCity) {
        this.departureCity = departureCity;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public City getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(City arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
}
