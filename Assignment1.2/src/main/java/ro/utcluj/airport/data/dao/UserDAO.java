package ro.utcluj.airport.data.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.omg.PortableInterceptor.USER_EXCEPTION;
import ro.utcluj.airport.data.HibernateUtil;
import ro.utcluj.airport.data.model.Flight;
import ro.utcluj.airport.data.model.User;

import java.util.List;

/**
 * @author catalin
 */
public class UserDAO {

    private static final Logger LOGGER = LogManager.getLogger(UserDAO.class);

    @SuppressWarnings("unchecked")
    public User login(String username, String password) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        List<User> user = null;
        try {
            tx = session.beginTransaction();

            Query query = session.createQuery("FROM User Where username = :username AND password = :password");
            query.setParameter("username", username);
            query.setParameter("password", password);

            user = query.list();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("Error in reading user.", e);
        } finally {
            session.close();
        }
        return user != null && !user.isEmpty() ? user.get(0) : null;
    }

    public User saveUser(User user) {
        int userId = -1;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            userId = (Integer) session.save(user);
            user.setId(userId);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("Error in saving user", e);
        } finally {
            session.close();
        }
        return user;
    }
}
