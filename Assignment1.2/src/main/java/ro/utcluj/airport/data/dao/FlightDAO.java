package ro.utcluj.airport.data.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import ro.utcluj.airport.data.HibernateUtil;
import ro.utcluj.airport.data.model.Flight;

import java.util.List;

/**
 * @author catalin
 */
public class FlightDAO {

    private static final Logger LOGGER = LogManager.getLogger(FlightDAO.class);

    public void saveFlight(Flight flight) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.saveOrUpdate(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", e);
        } finally {
            session.close();
        }
    }

    @SuppressWarnings("unchecked")
    public List<Flight> findFlights() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        try {
            tx = session.beginTransaction();
            flights = session.createQuery("FROM Flight").list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("Error in reading flights.", e);
        } finally {
            session.close();
        }
        return flights;
    }

    @SuppressWarnings("unchecked")
    public Flight findFlight(int id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        List<Flight> flights = null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE id = :id");
            query.setParameter("id", id);
            flights = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("Error in reading flight", e);
        } finally {
            session.close();
        }
        return flights != null && !flights.isEmpty() ? flights.get(0) : null;
    }

    public Flight deleteFlight(int id) {

        Flight flight = findFlight(id);

        if (flight == null) {
            return null;
        }

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(flight);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("Unable to delete flight", e);
        } finally {
            session.close();
        }
        return flight;
    }
}
