package ro.utcluj.airport.business;

import com.mysql.cj.api.Session;
import ro.utcluj.airport.data.dao.CityDAO;
import ro.utcluj.airport.data.dao.FlightDAO;
import ro.utcluj.airport.data.model.City;
import ro.utcluj.airport.data.model.Flight;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author catalin
 */
public class FlightServlet extends HttpServlet {
    private static String LIST_FLIGHTS = "/flights.jsp";
    private FlightDAO dao;

    public FlightServlet() {
        super();
        dao = new FlightDAO();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String forward = "";
        String action = request.getParameter("action");

        HttpSession session = request.getSession(false);
        boolean isAdmin = (Boolean)session.getAttribute("isAdmin");

        if (action != null && isAdmin) {
            if (action.equalsIgnoreCase("delete")) {
                int id = Integer.parseInt(request.getParameter("id"));
                dao.deleteFlight(id);
                forward = LIST_FLIGHTS;
                request.setAttribute("flights", dao.findFlights());
            } else if (action.equalsIgnoreCase("new")) {
                CityDAO cityDAO = new CityDAO();
                List<City> cities = cityDAO.findCities();
                request.setAttribute("cities", cities);
                forward = "new_flight.jsp";
            } else if (action.equalsIgnoreCase("edit")) {
                CityDAO cityDAO = new CityDAO();
                List<City> cities = cityDAO.findCities();
                request.setAttribute("cities", cities);
                Flight flight = dao.findFlight(Integer.parseInt(request.getParameter("id")));
                request.setAttribute("flight", flight);
                forward = "new_flight.jsp";
            }
        } else {
            forward = LIST_FLIGHTS;
            request.setAttribute("flights", dao.findFlights());
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Flight flight = new Flight();
        flight.setAirplaneType(request.getParameter("airplaneType"));

        CityDAO cityDAO = new CityDAO();

        City departureCity = cityDAO.findCity(Integer.parseInt(request.getParameter("departureCity")));
        flight.setDepartureCity(departureCity);

        try {
            Date departureTime = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(request.getParameter("departureDate"));
            flight.setDepartureTime(departureTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        City arrivalCity = cityDAO.findCity(Integer.parseInt(request.getParameter("arrivalCity")));
        flight.setArrivalCity(arrivalCity);

        try {
            Date arrivalTime = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(request.getParameter("arrivalDate"));
            flight.setArrivalTime(arrivalTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String id = request.getParameter("flightId");
        if (id == null || id.equals("")) {
            dao.saveFlight(flight);
        } else {
            flight.setId(Integer.parseInt(id));
            dao.saveFlight(flight);
        }
        RequestDispatcher view = request.getRequestDispatcher(LIST_FLIGHTS);
        request.setAttribute("flights", dao.findFlights());
        view.forward(request, response);
    }

}
