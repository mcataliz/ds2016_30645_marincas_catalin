package ro.utcluj.airport.business;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author catalin
 */
public class LoginFilter implements Filter {


    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);
        String loginURI = request.getContextPath() + "/login.html";

        boolean loggedIn = session != null && session.getAttribute("logged.user") != null;
        boolean loginRequest = request.getRequestURI().contains("login");

        String uri = request.getRequestURI();

        if (loggedIn || loginRequest) {
            chain.doFilter(request, response);
        } else if (uri.indexOf("/css") > 0) {
            chain.doFilter(request, response);
        } else if (uri.indexOf("/img") > 0) {
            chain.doFilter(request, response);
        } else if (uri.indexOf("/js") > 0) {
            chain.doFilter(request, response);
        } else {
            response.sendRedirect(loginURI);
        }
    }

    public void init(FilterConfig filterConfig) throws ServletException {
    }

    public void destroy() {
    }
}
