package ro.utcluj.airport.business;


import ro.utcluj.airport.data.dao.UserDAO;
import ro.utcluj.airport.data.model.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author catalin
 */
public class LoginServlet extends HttpServlet {

    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        res.setContentType("text/html");

        String username = req.getParameter("username");
        String password = req.getParameter("password");

        User user = allowUser(username, password);

        if (user == null) {
            res.sendRedirect(req.getContextPath() + "/login.html");
        } else {
            HttpSession session = req.getSession();
            session.setAttribute("logged.user", username);
            session.setAttribute("isAdmin", user.isAdmin());
            res.sendRedirect(req.getContextPath() + "/flights");
        }
    }

    private User allowUser(String username, String password) {
        UserDAO userDAO = new UserDAO();
        User u = userDAO.login(username,password);

        return u;
    }
}
