package ro.utcluj.airport.business;

import ro.utcluj.airport.data.dao.FlightDAO;
import ro.utcluj.airport.data.model.Flight;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * @author catalin
 */
public class LocalTimeServlet extends HttpServlet {

    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {

        FlightDAO flightDAO = new FlightDAO();

        Flight flight = flightDAO.findFlight(Integer.parseInt(req.getParameter("id")));

        double departureCityLat = flight.getDepartureCity().getLatitude();
        double departureCityLong = flight.getDepartureCity().getLongitude();

        double arrivalCityLat = flight.getArrivalCity().getLatitude();
        double arrivalCityLong = flight.getArrivalCity().getLongitude();

        URL departureUrl = new URL("http://new.earthtools.org/timezone/"+ departureCityLat + "/" + departureCityLong);
        URL arrivalUrl = new URL("http://new.earthtools.org/timezone/"+ arrivalCityLat + "/" + arrivalCityLong);

        URLConnection departureCon = departureUrl.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(
                departureCon.getInputStream()));
        String inputLine;
        String xml = "";
        while ((inputLine = in.readLine()) != null)
            xml += inputLine;
        in.close();

        req.setAttribute("departureLT", xml.split("<localtime>")[1].split("</localtime>")[0]);

        URLConnection arrivalCon = arrivalUrl.openConnection();
        in = new BufferedReader(new InputStreamReader(
                arrivalCon.getInputStream()));
        xml = "";
        while ((inputLine = in.readLine()) != null)
            xml += inputLine;
        in.close();

        req.setAttribute("arrivalLT", xml.split("<localtime>")[1].split("</localtime>")[0]);
        req.setAttribute("flightId", flight.getId());
        req.setAttribute("depCity", flight.getDepartureCity().getName());
        req.setAttribute("arrivCity", flight.getArrivalCity().getName());
        RequestDispatcher forward = req.getRequestDispatcher("/flights");
        forward.forward(req, res);
    }
}
