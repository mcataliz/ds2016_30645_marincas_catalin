package ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;

/**
 * @author  catalin
 */
public interface IPriceService {
    /**
     * Computes the selling price of a Car.
     *
     * @param c Car for which to compute the selling price
     * @return tax for the car
     */
    double computeSellingPrice(Car c);
}
