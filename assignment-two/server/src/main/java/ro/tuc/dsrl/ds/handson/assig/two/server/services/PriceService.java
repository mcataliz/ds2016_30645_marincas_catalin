package ro.tuc.dsrl.ds.handson.assig.two.server.services;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.IPriceService;

/**
 * @author catalin
 */
public class PriceService implements IPriceService {

    @Override
    public double computeSellingPrice(Car c) {
        if (c.getEngineCapacity() <= 0) {
            throw new IllegalArgumentException("Engine capacity must be positive.");
        }

        double price = 2015 - c.getYear();
        price = c.getPurchasingPrice() / 7 * price;
        price = c.getPurchasingPrice() - price;

        if (price < 0) price = -1 * price;

        return price;
    }
}
