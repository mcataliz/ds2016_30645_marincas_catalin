package sample.Common;

import java.io.Serializable;

/**
 * @author catalin
 */
public class Car implements Serializable {
    private static final long serialVersionUID = 1L;
    private int engineSize;
    private int year;
    private double price;

    public Car(int engineSize, int year, double price) {
        this.engineSize = engineSize;
        this.year = year;
        this.price = price;
    }

    public int getEngineSize() {
        return engineSize;
    }

    public void setEngineSize(int engineSize) {
        this.engineSize = engineSize;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + engineSize;
        result = prime * result + year;
        result = prime * result + (int)price;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Car other = (Car) obj;
        if (engineSize != other.engineSize)
            return false;
        if (year != other.year)
            return false;
        if (price != other.price)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Car [year=" + year + ", engineCapacity=" + engineSize + ", purchasingPrice=" + price + "]";
    }

}
