package sample.Common;

import sample.Common.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author catalin
 */
public interface ICarService extends Remote {

    double computeSellingPrice(Car c) throws RemoteException;
    double calculateTax(Car c) throws RemoteException;
}
