package sample.Server;

import sample.Common.ICarService;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * @author catalin
 */
public class Server {

    public static void main(String args[]) {
        try {
            Registry registry = LocateRegistry.createRegistry(1099);
            ICarService carService = new CarService();
            registry.bind("CarService", carService);

            System.out.println("Server is ready...");
        } catch (Exception e) {
            System.out.println("CarService err: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
