package sample.Server;

import sample.Common.Car;
import sample.Common.ICarService;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * @author catalin
 */
public class CarService extends UnicastRemoteObject implements ICarService {

    protected CarService() throws RemoteException {
    }

    public double computeSellingPrice(Car c) throws RemoteException {
        double price = c.getPrice();
        double result = price - price / 7 * (2015 - c.getYear());
        return result < 1 ? -result : result;
    }

    public double calculateTax(Car c) throws RemoteException {
        int sum;
        int engine = c.getEngineSize();

        if (engine>3001){
            sum = 290;
        } else if (engine>2600){
            sum = 144;
        } else if (engine>2000){
            sum = 72;
        } else if (engine>1600){
            sum = 18;
        } else {
            sum = 8;
        }
        return engine / 200 * sum;
    }
}
