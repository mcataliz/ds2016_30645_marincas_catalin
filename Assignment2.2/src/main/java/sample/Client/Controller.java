package sample.Client;

import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import sample.Common.Car;
import sample.Common.ICarService;

import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.text.DecimalFormat;

public class Controller {
    public TextField engineSize;
    public TextField year;
    public TextField price;
    public Label result;
    public Label message;

    private static DecimalFormat df2 = new DecimalFormat("###.##");

    public void calculateTax(ActionEvent actionEvent) {
        try {
            Registry registry = LocateRegistry.getRegistry(1099);
            ICarService carService = (ICarService) registry.lookup("CarService");

            Car car = createCar();
            double res = carService.calculateTax(car);
            result.setText(df2.format(res) + "");
        } catch (NumberFormatException e) {
            message.setText("Please enter valid numbers.");
        } catch (AccessException e) {
            message.setText("Error communicating with the server.");
        } catch (RemoteException e) {
            message.setText("Error communicating with the server.");
        } catch (NotBoundException e) {
            message.setText("Error communicating with the server.");
        }
    }

    public void calculateSellingPrice(ActionEvent actionEvent) {
        try {
            Registry registry = LocateRegistry.getRegistry(1099);
            ICarService carService = (ICarService) registry.lookup("CarService");

            Car car = createCar();
            double res = carService.computeSellingPrice(car);
            result.setText(df2.format(res) + "");
        } catch (NumberFormatException e) {
            message.setText("Please enter valid numbers.");
        } catch (AccessException e) {
            message.setText("Error communicating with the server.");
        } catch (RemoteException e) {
            message.setText("Error communicating with the server.");
        } catch (NotBoundException e) {
            message.setText("Error communicating with the server.");
        }
    }

    private Car createCar() throws NumberFormatException {
        int engineSie = Integer.parseInt(engineSize.getText());
        int carYear = Integer.parseInt(year.getText());
        double sellingPrice = Double.parseDouble(price.getText());

        return new Car(engineSie, carYear, sellingPrice);
    }
}
